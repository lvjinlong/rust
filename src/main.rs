extern crate rand; // 通知 Rust 我们要使用外部依赖。这也会调用相应的

use std::io;
use std::cmp::Ordering;
use rand::{thread_rng, Rng};

fn main() {
    println!("Guess the number!");

    let mut rng = thread_rng();
    let secret_number = rng.gen_range(1, 101);

    loop {
        println!("Please input your guess.");

        // 创建一个地方储存用户输入
        // 使用 mut 来使一个变量可变
        // ::调用关联函数 => new是String的一个关联函数
        let mut guess = String::new();

        // 如果开头没有std::io，则本句可写为std::io::stdin
        // & 表示这个参数是一个 引用，默认是不可变的，需要写成 &mut guess 而不是 &guess 来使其可变。
        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        let guess_trim = guess.trim();

        // 无符号的 32 位整型
        let guess_number: u32 = match guess_trim.parse() {
            Ok(num) => num,
            // _ 是一个通配值，本例中用来匹配所有 Err 值
            Err(_) => {
                println!("Please type a number!");
                continue;
            }
        };
        // .expect("Please type a number!"); 会导致程序崩溃，所以使用更优雅的办法

        println!("You guessed: {}", guess_trim);

        match guess_number.cmp(&secret_number) {
            Ordering::Less    => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal   => {
                println!("You win!");
                break;
            }
        }
    }
}
